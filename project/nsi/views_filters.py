from url_filter.filtersets import ModelFilterSet
from project.nsi import models as nsi_models


class SignDetectMethodFilterSet(ModelFilterSet):
    class Meta(object):
        model = nsi_models.SignDetectMethod
