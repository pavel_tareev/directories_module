from django.urls import path
from project.nsi import views

urlpatterns = [
    path('directories/', views.DirectoriesListView.as_view(), name='nsi_directories_url'),
    path('directories/<str:alias>/', views.DirectoryListView.as_view(), name='nsi_directory_list_url'),
    path('directories/<str:alias>/<int:pk>/', views.DirectoryItemDetailView.as_view(),
         name='nsi_directory_detail_url'),
]
