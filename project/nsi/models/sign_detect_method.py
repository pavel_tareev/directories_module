from django.utils.translation import ugettext_lazy as _
from project.nsi.mixins.models import SimpleDirectoryMixin


class SignDetectMethod(SimpleDirectoryMixin):
    # REQUIRED PROPERTIES
    alias = 'sys-sign-detect-methods'
    serializer_class_name = 'SignDetectMethodSerializer'
    url_filter_class_name = 'SignDetectMethodFilterSet'
    # fields

    class Meta:
        db_table = 'nsi_sign_detect_method'
        verbose_name = _('Способ выявления признака')
        verbose_name_plural = _('Способы выявления признака')
        unique_together = ('code', 'name',)
