# Generated by Django 3.0.4 on 2020-07-28 14:37

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SignDetectMethod',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('name', models.TextField(verbose_name='Наименование')),
                ('is_active', models.BooleanField(default=True, verbose_name='Является активным')),
                ('code', models.CharField(max_length=125, verbose_name='Код')),
                ('short_name', models.TextField(blank=True, verbose_name='Краткое наименование')),
                ('created_by', models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='created_nsi_signdetectmethod_set', to=settings.AUTH_USER_MODEL, verbose_name='Автор')),
                ('modified_by', models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='modified_nsi_signdetectmethod_set', to=settings.AUTH_USER_MODEL, verbose_name='Изменил')),
            ],
            options={
                'verbose_name': 'Способ выявления признака',
                'verbose_name_plural': 'Способы выявления признака',
                'db_table': 'nsi_sign_detect_method',
                'unique_together': {('code', 'name')},
            },
        ),
    ]
