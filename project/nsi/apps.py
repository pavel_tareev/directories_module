from django.apps import AppConfig


class NsiConfig(AppConfig):
    name = 'project.nsi'
    verbose_name = 'Нормативно-справочная информация'
