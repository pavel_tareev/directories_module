from rest_framework import generics
from rest_framework.response import Response

from project.nsi import helpers
from project.nsi.mixins.views import DirectoriesViewMixin


class DirectoriesListView(DirectoriesViewMixin, generics.ListAPIView):
    def get(self, request, *args, **kwargs):
        directories = helpers.get_all_dictionaries(request)

        serializer = self.get_serializer(data=directories, many=True)
        serializer.is_valid(raise_exception=True)

        if request.query_params.get('page', '') == 'all':
            results = {
                "count": len(serializer.validated_data),
                "next": None,
                "previous": None,
                "results": serializer.validated_data
            }
        else:
            results = self.paginate_results(serializer.validated_data, request)

        return Response(results)


class DirectoryListView(DirectoriesViewMixin, generics.ListAPIView):
    def get(self, request, *args, **kwargs):
        self.set_serializer_queryset_and_filter_from_alias()

        if request.query_params.get('page', '') == 'all':
            serializer = self.get_serializer(self.get_queryset(), many=True)
            return Response({
                "count": len(serializer.data),
                "next": None,
                "previous": None,
                "results": serializer.data
            })
        else:
            return super().get(request, *args, **kwargs)


class DirectoryItemDetailView(DirectoriesViewMixin, generics.RetrieveAPIView):
    def get(self, request, *args, **kwargs):
        self.set_serializer_queryset_and_filter_from_alias()

        instance = self.get_object()
        serializer = self.get_serializer(instance)

        return Response(serializer.data)
