from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from model_utils.models import TimeStampedModel


class CreatedUpdatedBy(TimeStampedModel):
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE,
                                   related_name='created_%(app_label)s_%(class)s_set', editable=False,
                                   verbose_name='Автор')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE,
                                    related_name='modified_%(app_label)s_%(class)s_set', editable=False,
                                    verbose_name='Изменил')

    class Meta:
        abstract = True


class AbstractBaseMixin(CreatedUpdatedBy):
    """
    Base model for all directories models
    """
    name = models.TextField(verbose_name=_('Наименование'))
    is_active = models.BooleanField(default=True, verbose_name=_('Является активным'))

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class CodeMixin(models.Model):
    code = models.CharField(max_length=125, verbose_name=_('Код'))

    class Meta:
        abstract = True


class ShortNameMixin(models.Model):
    short_name = models.TextField(blank=True, verbose_name=_('Краткое наименование'))

    class Meta:
        abstract = True


class SimpleDirectoryMixin(ShortNameMixin, CodeMixin, AbstractBaseMixin):
    """
    Base model mixin for simple directories
    """

    @property
    def alias(self):
        raise NotImplementedError

    @property
    def serializer_class_name(self):
        raise NotImplementedError

    @property
    def url_filter_class_name(self):
        raise NotImplementedError

    class Meta:
        abstract = True
