from rest_framework.permissions import IsAuthenticated, AllowAny
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.urls import reverse

from project.nsi import models, serializers, helpers


class DirectoriesViewMixin:
    queryset = models.SignDetectMethod.objects.none()  # just a stub
    permission_classes = [AllowAny] if settings.OFF_AUTH else [IsAuthenticated]
    serializer_class = serializers.DirectoriesSerializer

    def paginate_results(self, directories: list, request):
        """
        Function wrapper for paginate custom dict lists
        """
        page = request.GET.get('page', 1)
        paginator = Paginator(directories, settings.REST_FRAMEWORK['PAGE_SIZE'])

        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            results = paginator.page(1)
        except EmptyPage:
            results = paginator.page(paginator.num_pages)

        return {
            'count': len(results.object_list),
            'next': '{}?page={}'.format(request.build_absolute_uri(reverse('nsi_directories_url')),
                                        results.next_page_number()) if results.has_next() else None,
            'previous': '{}?page={}'.format(request.build_absolute_uri(reverse('nsi_directories_url')),
                                            results.previous_page_number()) if results.has_previous() else None,
            'results': results.object_list
        }

    def set_serializer_queryset_and_filter_from_alias(self):
        dictionary = helpers.get_dictionary(self.request, self.kwargs['alias'])
        # override serializers and model
        self.serializer_class = dictionary['serializer_class']
        self.queryset = dictionary['model'].objects.all()
        self.filter_class = dictionary['filter_class']
