from pydoc import locate
from django.apps import apps
from django.urls import reverse
from rest_framework.exceptions import ParseError, NotFound


def get_all_dictionaries(request) -> list:
    all_directories = []

    for model in apps.get_app_config('nsi').get_models():
        serializer_class = locate('project.nsi.serializers.{}'.format(model.serializer_class_name))
        filter_class = locate('project.nsi.views_filters.{}'.format(model.url_filter_class_name))

        if not serializer_class:
            raise ParseError('Класс сериализатора для таблицы {} не найден!'.format(model._meta.db_table))

        if not filter_class:
            raise ParseError('Класс фильтров для таблицы {} не найден!'.format(model._meta.db_table))

        all_directories.append({
            'alias': model.alias,
            'table_name': model._meta.db_table,
            'name': '{}'.format(model._meta.verbose_name_plural),
            'url': request.build_absolute_uri(reverse('nsi_directory_list_url', args=[model.alias])),
            'serializer_class': serializer_class,
            'model': model,
            'filter_class': filter_class
        })

    return all_directories


def get_dictionary(request, alias: str):
    for dictionary in get_all_dictionaries(request):
        if dictionary.get('alias', '') == alias:
            return dictionary

    raise NotFound('Справочник {} не найден!'.format(alias))
