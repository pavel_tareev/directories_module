from rest_framework import serializers


class DirectoriesSerializer(serializers.Serializer):
    alias = serializers.CharField(max_length=255)
    table_name = serializers.CharField(max_length=255)
    name = serializers.CharField(max_length=255)
    url = serializers.URLField()
