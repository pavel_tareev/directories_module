from rest_framework import serializers

from project.nsi.models import SignDetectMethod


class SignDetectMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = SignDetectMethod
        fields = '__all__'
